#pragma once

#define DMACRO_CheckModuleVersion(MModuleName, MMinVersion, MMaxVersion)\
        (MODULE__##MModuleName##__Version >= MMinVersion &&\
         MODULE__##MModuleName##__Version <= MMaxVersion)
